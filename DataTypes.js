console.log("Vars example: ");
const _name = "Liza";//string
//console.log(typeof _name);
const _age = 19;//number
//console.log(typeof _age);
const _isStudent = true;//boolean
//console.log(typeof _isStudent);
console.log(`My name is ${_name} and I am ${_age} years old`);
if(_isStudent) {
    console.log("I'm a student");
}
else {
    console.log("I'm not a student");
}

console.log("\nObject example:");
var human = {_name:"Ford", _age:18, _isStudent:false};
//console.log(typeof human);
for(i in human){
    console.log(`${i}: ${human[i]}`);
}

console.log("\nArray example:");
const numbers = [1,3,5,7,9,10];
//console.log(typeof numbers);
var sum = 0;

for(i = 0;i<numbers.length;i++){
    if  (numbers[i]%2!=0){
        sum = sum+numbers[i];
    }
}

console.log(`Array: ${numbers}`);
console.log(`Sum of array elements: ${sum}`);


console.log("\nOperators examples: ");
var isAdult = function(hum) {
    console.log(`Are you an adult? Your age is ${hum._age}`);
    if(hum._age > 18){
        console.log("Yes, you are an adult");
    }
    else if(hum._age < 18){
        console.log("No, you are not an adult");
    }
    else if(hum._age == 18){
        console.log("You are a young adult!");
    }
};

isAdult(human);


